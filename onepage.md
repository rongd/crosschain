---
title: "Disulfide: An Interoperability Protocol for Heterogeneous Blockchains"
---

# Write Operation

`Write operation` is provided for cross chain transactions (e.g. asset transfer, contract call ...).

```mermaid
sequenceDiagram
    participant U as USER
    participant S as SRC_CCMC
    participant R as RELAYER
    participant D as DST_CCMC
    participant I as INTERMEDIATE
    U->>S: M, Q, Ce, Cr
    S-->>R: M, Q, Ce, Cr
    R->>D: Q, Ce, Cr, proof
    Note right of D: validate
    D->>I: Q
    Note right of I: execute Q
    R->>S: ask for pay
    Note right of S: validate
    S->>R: pay
```

1. `user` ^[`user` is short for `cross chain user`] initiates a `write operation` by calling `CCMC(src)` ^[`CCMC` is short for `cross chain management contract`; `CCMC(src)` means `CCMC` on `source chain` and `CCMC(dst)` means `CCMC` on `destination chain`]
2. `relayer` obtains the `write operation` from `CCMC(src)`
3. `relayer` send the operation and its proof to `CCMC(dst)`
4. `CCMC(dst)` executes the operation via the `intermediate contract` after validation
5. `relayer` sends the proof and gets paid

# Read Operation

`Read operation` is provided for cross chain status (e.g. balance, contract result ...) reading.

```mermaid
sequenceDiagram
    participant U as USER
    participant S as SRC_CCMC
    participant R as RELAYER
    participant D as DST_CHAIN
    U->>S: M, Q, Cr
    S-->>R: M, Q, Cr
    D-->>R: status
    R->>S: status, proof
    Note right of S: validate
    Note right of S: log status
    S->>R:  pay
```

1. `user` initiates a `read operation` by calling `CCMC(src)`
2. `relayer` obtains the `read operation` from `CCMC(src)`
3. `relayer` gets the status specified by the `read operation` from `destination chain`
4. `relayer` returns the status back to `CCMC(src)` with its proof
5. `CCMC(src)` logs the status and pays the relayer after validation

# Synchronize Block Header

It is necessary to synchronize block headers between `participant chain`s and `relay chain` for validation.

- `participant chain`s to `relay chain`: synchronize hash of block headers
- `relay chain` to `participant chain`s: synchronize key block headers

```mermaid
graph LR
    P[participant chian]
    R[relay chain]
    P -- hash of block header -->R
    R -- key block header -->P
```

# Atomic Cross Chain Transactions

