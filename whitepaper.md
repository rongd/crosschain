---
title: "Disulfide: An Interoperability Protocol for Heterogeneous Blockchains"
author:
- Wang Yong Qiang
- Su Yu Yang
- Meng Zhe Xuan
- Albert Acebrón
- Wang Hao Ran
- Shen Ze Yu
- Onchain & NGD Research
date: 2019-08-15
subtitle: Draft 1
include-before:
- '`\newpage{}`{=latex}'
---

# The Introduction

Blockchain technology has a significant achievement in our daily life. Finance ^[Zhao, J. Leon, Shaokun Fan, and Jiaqi Yan. "Overview of business innovations and research opportunities in blockchain and introduction to the special issue." (2016): 28.] ^[Tapscott, Alex, and Don Tapscott. "How blockchain is changing finance." Harvard Business Review 1.9 (2017): 2-5.], supply chain ^[Kshetri, Nir. "1 Blockchain’s roles in meeting key supply chain management objectives." International Journal of Information Management 39 (2018): 80-89.
], identity management ^[Zyskind, Guy, and Oz Nathan. "Decentralizing privacy: Using blockchain to protect personal data." 2015 IEEE Security and Privacy Workshops. IEEE, 2015.], digital assets ^[Crosby, Michael, et al. "Blockchain technology: Beyond bitcoin." Applied Innovation 2.6-10 (2016): 71.] ^[Community Yellow Paper: A Technical Specification for NEO Blockchain, Igor M. Coelho, Vitor N. Coelho, Peter Lin, Erik Zhang], and distributed storage ^[Benet, Juan. "Ipfs-content addressed, versioned, p2p file system." arXiv preprint arXiv:1407.3561 (2014).] ^[Public Version of the Research Plan for NeoFS: Distributed Decentralized Blockchain-based Storage Platform, JSC "NEO Saint Petersburg Competence Center"] all allow us to see infinite potential of a new generation of decentralized internet protocols. Blockchain can be a reshaping mechanism for social-economic operations. However, due to the massive development of blockchain, the phenomenon of information isolated island is re-emerging. Information exchange and asset replacement between blockchain infrastructures are limited. If blockchains is regarded as isolated computers, then it may go back to the nascent era of the internet in the 1990s. 

To create an actual blockchain network, we propose an interoperability protocol for heterogeneous blockchains based on cross chain management contracts which includes the following items: 

1. propose a solution to implement the interoperation between blockchains
2. propose a two-phase commit protocol for atomic cross chain transactions

This groundbreaking interoperability agreement has the following key features:

- Eco Friendly

  The agreement is designed for cross chain synchronization. Neither issuing tokens nor a dedicated smart contract system to ensure healthy competition and collaboration among member chains.

- Low Barrier to Entry

  The agreement aims to promote seamless access to technology, and there is no need to develop or adjust protocol layers when you are using existing blockchain projects.

- Trading Atomicity

  The agreement aims to achieve the ultimate and atomic nature of cross chain transactions, with a focus on cross chain smart contract interactions to extend the application scenarios for decentralized applications.

- Security Enhancement

  The agreement will add a complex set of mechanisms at the technical and operational levels to enhance the security of cross chain transactions and interactions.

## Previous work

The concept of blockchain transactions that are somehow connected across blockchains has been proposed several times under a wide variety of systems, with the first high-impact publication being Pegged Sidechains ^[Enabling Blockchain Innovations with Pegged Sidechains, Adam Back, Matt Corallo, Luke Dashjr, Mark Friedenbach, Gregory Maxwell, Andrew Miller, Andrew Poelstra, Jorge Timón, and Pieter Wuille, 2014], which proposed a protocol under which a coin or token could be transferred from a main chain to its child chains, allowing for heterogenous protocols to be integrated into a chain without requiring alterations in the main chain's protocol. The inter-chain interactions proposed by this system are limited to the use-case mentioned and therefore may the system unsuitable if other things are required.

A somewhat more advanced concept, Cosmos Hub ^[A network of Distributed Ledgers, https://cosmos.network/resources/whitepaper, retrieved on 18/7/2019], proposes a network of blockchains, each achieving consensus on its own using the Tendermint and a custom set of validators, that would be tied to a main chain which would enable cross chain transfers of assets.

Finally, Polkadot ^[POLKADOT: VISION FOR A HETEROGENEOUS MULTI-CHAIN FRAMEWORK, Gavin Wood] proposes a unification of consensus on its parachains along with a transaction routing mechanism which would allow for security pooling and arbitrary message passing between blockchains. 

# The Problem

In the following sections we formalize the concept of a blockchain and what it means for blockchain to have crosschain capabilities.

## Blockchain

A blockchain is essentially a distributed database of records, or public ledger of all transactions or digital events that have been executed and shared among participant parties. Each transaction in the public ledger is verified by consensus of a majority of the participants in the system. Once entered, information can never be erased. The blockchain contains a certain and verifiable record of every single transaction ever
made. ^[Crosby, Michael, et al. "Blockchain technology: Beyond bitcoin." Applied Innovation 2.6-10 (2016): 71.]

According to formal definitions of blockchain as a state machine in the literature ^[Blockchain State Machine Representation, Jamsheed Shorish, Version 1.0.1, January 19, 2018], a blockchain should be formalized as a probabilistic state machine due to the possible existence of conflicting forks. In this paper we will simplify this concept by adding the assumptions that blockchains will not fork (it would be possible to add requirements such as a having a minimum of 6 blocks confirmed on top of another block before considering such a block as part of the state machine in order to achieve this property on sone blockchains without finality like Bitcoin ^[Nakamoto, Satoshi. "Bitcoin: A peer-to-peer electronic cash system." (2008).] in practice). The assumption could be realized by goverance solutions in practice, such as the requirement of pledge and compensation for nodes. This assumption allows us to model the blockchain as a deterministic state machine, concretely as a Moore machine ^[Moore, Edward F (1956). "Gedanken-experiments on Sequential Machines". Automata Studies, Annals of Mathematical Studies. Princeton, N.J.: Princeton University Press (34): 129–153.] $M$:

$$
M = (\mathbb{S}, \mathbf{S}^{(0)}, \mathbb{I}, \mathbb{O}, f_{\mathtt{transition}}, f_{\mathtt{output}})
$$

The definition of each parameters will be introduced in the following sections.

A solution based on probabilistic state machines may be proposed in further research.

### State Domain

State domain $\mathbb{S}$ is the domain of states the blockchain can be in, where a state is defined as a sequence of all the blocks that have been included in the blockchain (the inclusion process is defined by the transition function) up until a point in time and a block is a sequence of transactions with a header. Therefore, $\forall \mathbf{S} \in \mathbb{S}$, $\mathbf{S}$ is an ordered subset of $\mathbb{I}$.

$$
\mathbb{S} = \mathbb{I}^{m}
$$

### Initial State

Initial state $\mathbf{S}^{(0)} \in \mathbb{S}$ is the start state of the state machine. which is an empty sequence.

$$
\mathbf{S}^{(0)} = []
$$

Blockchains that include a genesis block ^[Genesis Block, https://en.bitcoin.it/wiki/Genesis_block, retrieved on 16 July 2019] fit into this definition if we regard the genesis block as the first block to be added.

$$
\mathbf{S}^{(0)} = [I_{\mathtt{genesis}}]
$$

### Input Domain

Input domain $\mathbb{I}$ is the domain of correct blocks that can be generated and broadcasted. The exact definition of this concept may vary from blockchain to blockchain but in all these cases, it is the basic element that enables the modification of blockchain state. For this reason we will use the block as the basic unit upon which we will base the constructions of the other concepts.

We formally define block the following way:

$$
\mathbb{I} = \mathbb{H} \times \mathbb{X}^{m}
$$

Where $\mathbb{H}$ is the set of possible block headers, which are defined by a tuple of several values such as miner signature, transaction merkle root, state root and several others that may vary from blockchain to blockchain (see Bitcoin's Block Header ^[Block Headers, https://bitcoin.org/en/developer-reference#block-headers, retrieved on 16 July 2019] and Ethereum's Block Header ^[4.3 section, https://ethereum.github.io/yellowpaper/paper.pdf, retrieved on 16 July 2019] for detailed definitions of two particular cases), $\mathbb{X}$ is the set of valid transactions ^[Definition 1, Blockchain State Machine Representation, Jamsheed Shorish, Version 1.0.1, January 19, 2018] and $m \in \mathbb{N}$.

Therefore, $\forall I \in \mathbb{I}$, $I$ consists of the block header $H$ and a sequence of transactions $\mathbf{X}$. The header $H$ contains the proof of the sequence of transactions $\mathbf{X}$ and the output $O^{(t)} = f_{\mathtt{output}}(\mathbf{S}^{(t)})$ at block height $t$. In other word, some one can say that a transaction $X$ is confirmed at block height $t$ if one can provide the proof that matches the block header $H^{(t)}$ at block height $t$. This is usually called merkle root in many blockchains. Similarly, some one can also prove the status at a block height.(The definition of status and output is defined in the [Output Domain](#output-domain) section) This is usually called state root in many blockchains. In this paper we won't go deep in detail of block structures because different blockchains may have different implements, but probably their block headers contain such essentials.

$$
I = (H,\mathbf{X})
$$

For the rest of this paper, the following notations will be used:

We use $X \in \mathbf{X}$ to represent that there is an object $X$ in the sequence $\mathbf{X}$.

$$
X \in \mathbf{X} \rightarrow \exists i \in \mathbb{N}, X_i = X
$$

We use $X \in I$ to represent that the transaction $X$ is included in block $I$

$$
X \in I \rightarrow \exists \mathbf{X}, X \in \mathbf{X}, I = (H,\mathbf{X})
$$

We use $X \in \mathbf{S}^{(t)}$ to represent that the transaction $X$ has been confirmed by the blockchain at block height $t$

$$
X \in \mathbf{S}^{(t)} \rightarrow \exists I \in \mathbf{S}^{(t)}, X \in I
$$

In UTXO model, a transaction is a transfer of value between two users. ^["Unspent Transaction Output, UTXO - Bitcoin Glossary". bitcoin.org.] ^[Blockchain State Machine Representation, Jamsheed Shorish, Version 1.0.1, January 19, 2018] While in account model like ethereum or NEO, a transaction is identical to any distributed or OLTP transaction (TPP Council 2010) that acts on some data. ^[Wood, Gavin. "Ethereum: A secure decentralised generalised transaction ledger." Ethereum project yellow paper 151.2014 (2014): 1-32.] ^[Community Yellow Paper: A Technical Specification for NEO Blockchain, Igor M. Coelho, Vitor N. Coelho, Peter Lin, Erik Zhang] However, in our model, a transaction is a set of data passed to the output function signed by the sender.

### Output Domain

The output domain $\mathbb{O}$ is the domain of the results that can be obtained by evaluating the output function $f_{\mathtt{output}}$ on a state, resulting in a view of it that represents the circumstances on which new transactions will be executed. For example, in Bitcoin, this domain would be represented by $\mathbb{N}^{2^{160}}$ which would be the set of all possible balances for all possible addresses. Each blockchain has their own output domain (for example, UTXO model and account model have different output domains).

We use output to represent a item in the output domain and we use status to denote a specific view of the output. For example, in Bitcoin, all the balances of all accounts are output, while the balance of one account is a status.

### Transition Function

Transition function $f_{\mathtt{transition}} : \mathbb{S} \times \mathbb{I} \rightarrow \mathbb{S}$ maps a state and an input to the next state.

Blockchains organize data in blocks, and update the entries using an append-only structure. More precisely, data can only be added to the blockchain in *time-ordered sequential order*, and it cannot be removed or changed.

For a block $I$, applying $f_{\mathtt{transition}}$ will cause changes to state $\mathbf{S}$. As it is shown below, the current state (block sequence) $\mathbf{S}$ and the input (a new block) $I$ are the inputs of transition function $f_{\mathtt{transition}}$, while $n_{\mathbf{S}}$ is the length of $\mathbf{S}$.

$$
f = \lambda \mathbf{S} . \lambda I . \mathbf{S} + I
$$

$$
\mathbf{S} + I = [S_0, S_1, \dots, S_k, \dots, S_{n_{\mathbf{S}}}, I]
$$

### Output Function

Output function $f_{\mathtt{output}} : \mathbb{S} \rightarrow \mathbb{O}$.

Unlike with the transition function, the application of the output function represents the process of executing all the transactions contained in the blocks that a state is composed of. This execution will result in an $\mathbf{O} \in \mathbb{O}$ which represents the circumstances on which any transactions in the immediately next blocks would be executed. The especifics of what does this function do vary from blockchain to blockchain and depend on the capabilities of the blockchain's VM ^[Elrom, Elad. "NEO Blockchain and Smart Contracts." The Blockchain Developer. Apress, Berkeley, CA, 2019. 257-298.], it's transaction model and many more details, so, in order to remain general, the details of this function will stay unspecified and would have to be adapted for each blockchain.

## Cross Chain

Whereas in the first few years of the blockchain industry one may be forgiven for thinking that there would be only "one blockchain to rule them all", in recent times such a possibility has been receding further and further from reality. Within the public blockchain space, different projects have been staking out different regions of the tradeoff space between security, privacy, efficiency, flexibility, platform complexity, developer ease of use and even what could only be described as political values. In the private and consortium chain space, the notion that there exists different chains for different industries - and even different chains within the same industry - is even less controversial and arguably universally understood as obvious. In such a world, one natural question that emerges is: how do these chains interoperate? ^[Chain Interoperability Vitalik Buterin September 9, 2016]

To solve this problem, we start by giving a definition of chain interoperability, also known as cross chain, which we divide into two types: write and read operations. All high level operations (such as asset transfer, asset exchange, cross chain contract call, etc.) can be contructed using these two basic building blocks.

Here is an example of a cross chain assets transfer (solid arrows are used to represent write operations while dotted arrow are for read operations):

1. Some assets are locked on chain A.
2. A cross chain call is performed from chain A to chain B to release assets there. This is a write operation.
3. Before releasing the assets on chain B, a check is performed to ensure that the assets on chain A have been locked correctly. This is a read operation.
4. If the check passes, the assets are released on chain B.
5. If it does not, the locked assets on chain A can be unlocked after checking that the releasing progress failed on chain B. This step requires a read operation.

```{.mermaid caption="Example of Cross Chain Assets Transfer Based on Write & Read Operation"}
sequenceDiagram
    participant A as ChainA
    participant B as ChainB
    Note left of A: lock assets
    A->>B: release assets
    B-->>A: ensure assets are locked
    alt success?
        Note right of B: release assets
    else
        A-->>B: release successful?
        Note left of A: check release status
        Note left of A: unlock assets
    end
```

The following terminology will be used in the definitions of the read and write operations:

source chain

:   the chain that initiating the cross chain operation

destination chain

:   target chain of the cross chain operation

participant chain

:   source chain or destination chain; in the rest of this paper, we use $\mathcal{M}$ to denote all participant chains

cross chain user

:   the user who wants to do a cross chain operation and owns an address on the source chain. It can only send transactions on the source chain but it wants to interoperate with the destination chain.

### Write Operation

A write operation is an operation that will affect the state of the destination chain. For example, calling a contract on the destination chain from the source chain is a write operation because it needs to append a transaction to the destination chain's state.

For the rest of this paper, $Q_{\mathtt{write}}$ will denote a write operation.

A write operation is finished if the transaction $X_{\mathtt{dst}}$ which contains the write operation $Q_{\mathtt{write}}$ has been confirmed by the destination chain at block height $t_{\mathtt{dst}}$.

$$
\exists t_{\mathtt{dst}} \in \mathbb{N},
X_{\mathtt{dst}} \in \mathbf{S}_{\mathtt{dst}}^{(t_{\mathtt{dst}})}
$$

Note that a finished write operation may not be a successful write operation because the transaction $X_{\mathtt{dst}}$ may have failed. If a transaction has failed, it is still included in a block and confirmed, which means the state has been changed by the transaction but the output has not.

### Read Operation

A read operation is any operation that will not affect the state of the destination chain but it will return back the status (a specific view of the output) of the destination chain.

For the rest of this paper, $Q_{\mathtt{read}}$ will denote a read operation.

A cross chain user can get the status of the destination chain at a specific block height $t_{\mathtt{dst}}$ from the source chain by using a read operation, receiving a specific view of the output $O_{\mathtt{dst}}^{(t_{\mathtt{dst}})}$.

For example, if a cross chain user wants to know the balance of his account on the destination chain a read operation would be used.

# The Solution

We propose the following solution to solve the cross chain problem. The solution consists of four parts.

The first two parts of the solution are [Write Operation Solution](#write-operation-solution) and [Read Operation Solution](#read-operation-solution), in which we introduce relayer entity. For write operation solution, relayer would help transfer the write operation from the source chain to the destination chain. For read operation solution, relayer would help transfer the status on the destination chain to the source chain. Whether relayer do correctly could be validated by both the source chain and the destination chain, which leads no trust on the relayer.

Actually, the scenario of write operation and read operation could be extended to one source chain interoperating with multiple destination chains. For the sake of simplicity, only the scenario of one source chain and one destination chain would be described below, as the extendsion is easy to realize.

Moreover, The write operation solution and read operation solution are based on a mechanism of synchronizing block header, which is introduced in the [Synchronize Block Header](#synchronize-block-header) section, and we propose a two-phase protocol for atomic cross chain transaction based on the write operation to ensure the final consistency of status of chains involved in the cross chain operations.

## Write Operation Solution

Currently, transactions in most blockchains require the existence of a sender address, so, in order to minimize the changes to the underlying protocol needed for cross chain, cross chain transactions will also contain a sender address, which will be the cross chain user itself on the source chain and an entity called relayer on the destination chain.

The main idea behind our solution for write operations is that a relayer entity will help the cross chain user transfer the write operation from the source chain to the destination chain.

This relayer mentioned previously, which could be any node (including the cross chain user itself) that is connected to the destination chain as well as the source chain, will simply relay some transactions from one chain into the other, without requiring any trust due to the validation checks performed at several stages of the protocol.

The details of each step will be discussed in the following sections.

```{.mermaid caption="Write Operation Overview"}
graph LR
    S[SRC CHAIN]
    D[DST CHAIN]
    R((RELAYER))
    S-- Q -->R
    R-- Q -->D
```

### How is a Cross Chain Write Operation Initiated on the Source Chain?

To initiate a cross chain write operation on the source chain, a cross chain user must send a transaction $X_{\mathtt{src}}$ on the source chain, calling the cross chain management contract and providing the write operation $Q_{\mathtt{write}}$ to perform on the destination chain $M_{\mathtt{dst}}$, which will be logged. 

After the cross chain management contract is called, the transaction $X_{\mathtt{src}}$ is confirmed on the source chain $M_{\mathtt{src}}$ at the block height $t_{\mathtt{src}}$, which means,

$$
X_{\mathtt{src}} \in \mathbf{S}_{\mathtt{src}}^{(t_{\mathtt{src}})}
$$

The destination chain $M_{\mathtt{dst}}$ and the write operation $Q_{\mathtt{write}}$ can be inferred from the transaction $X_{\mathtt{src}}$ after its successful execution.

Example:

1. A cross chain user calls a contract on the source chain.
2. The contract needs to do a cross chain write operation, so it calls the cross chain management contract and tells it the destination blockchain $M_{\mathtt{dst}}$ and the write operation $Q_{\mathtt{write}}$.

```{.mermaid caption="Initiate a Write Operation"}
graph TD
    U[Cross Chain User]
    subgraph SRC_CHAIN
        C[Contract]
        M[Cross Chain Management Contract]
    end
    U-->C
    C-- M_dst, Q -->M
```

### How does the relayer get the Cross Chain Write Operation from the Source Chain?

Only if the transaction $X_{\mathtt{src}}$ is executed successfully, the cross chain management contract will log the destination blockchain $M_{\mathtt{dst}}$ and the write operation $Q_{\mathtt{write}}$, which will be obtained by a relayer when it retrieves the log of the cross chain management contract. In other words, the relayer gets such infomation from the output $O_{\mathtt{src}}^{(t_{\mathtt{src}})}$ of the source chain at block height $t_{\mathtt{src}}$.

The following diagrams shows the relayer retrieving the destination blockchain $M_{\mathtt{dst}}$ and the write operation $Q_{\mathtt{write}}$ from the cross chain management contract:

```{.mermaid caption="Obtain Write Operation"}
graph LR
    R[Relayer]
    subgraph SRC_CHAIN
        M[Cross Chain Management Contract]
    end
    M-- M_dst, Q -->R
```

### How does the relayer send the Cross Chain Write Operation to the Destination Chain?

When a relayer gets a cross chain write operation from the source chain, the destination blockchain $M_{\mathtt{dst}}$ and the write operation $Q_{\mathtt{write}}$ will be extracted.

Afterwards, such relayer would send a transaction $X_{\mathtt{dst}}$ on the destination chain. This transaction, $X_{\mathtt{dst}}$, must call the cross chain management contract and provide it the write operation $Q_{\mathtt{write}}$, which will be executed by it after the validation process.

So after the cross chain management contract is called, the transaction $X_{\mathtt{dst}}$ is confirmed on the destination chain $M_{\mathtt{dst}}$ at the block height $t_{\mathtt{dst}}$, which means,

$$
X_{\mathtt{dst}} \in \mathbf{S}_{\mathtt{dst}}^{(t_{\mathtt{dst}})}
$$

The following diagram illustrates the steps that will be taken if $Q_{\mathtt{write}}$ is a cross chain contract call:

1. A relayer calls the cross chain management contract and provides the write operation $Q_{\mathtt{write}}$ to the cross chain management contract.
2. The cross chain contract executes the write operation $Q_{\mathtt{write}}$, which leads to a contract call.

```{.mermaid caption="Transfer Write Operation"}
graph LR
    R[Relayer]
    subgraph DST_CHAIN
        C[Contract]
        M[Cross Chain Management Contract]
    end
    R-- Q -->M
    M-->C
```

### How to Validate the Cross Chain Write Operation on Destination Chain?

When the cross chain management contract is called by the relayer, who must provide both the write operation $Q_{\mathtt{write}}$ and a proof of its inclusion on the source chain, the contract will first validate the write operation $Q_{\mathtt{write}}$ using the block header $H_{\mathtt{src}}^{(t_{\mathtt{src}})}$ on the source chain at the block height $t_{\mathtt{src}}$, which should be straightforward if it is able to retrieve the valid block header of the source chain.

Therefore, the relayer must provide both the write operation $Q_{\mathtt{write}}$ and a proof of its inclusion on the source chain to the cross chain management contract. And the cross chain management contract can validate it by the block header $H_{\mathtt{src}}^{(t_{\mathtt{src}})}$ on the source chain at the block height $t_{\mathtt{src}}$.

But how can a contract on the destination chain retrieve the block header $H_{\mathtt{src}}^{(t_{\mathtt{src}})}$ on the source chain? The system used to obtain such capability will be discussed in section [Synchronize Block Header](#synchronize-block-header), for now it will just be assumed it's possible.

### Who will pay for the Cross Chain Write Operation?

A transaction fee should be paid when executing transactions in any blockchain, and given that the solution discussed involves many transactions on different chains, there must be entities responsible for paying these. on the source chain, the cross chain user is the one sending the transaction $X_{\mathtt{src}}$, so the burden of paying for it falls on him, while, on the destination chain, the relayer will be the one sending the transaction $X_{\mathtt{dst}}$ and covering it's cost.

Given that the sending $X_{\mathtt{dst}}$ on the destination chain is not going to benefit the relayer directly, there should be some incentives to make relayers be willing to do their work, incentives which may cover the transaction fee.

For this reason, when a cross chain user initiates a write operation on the source chain, the user can set a reward which will be awarded to a relayer upon completion of its job.

But what if the relayed write operation fails because of gas exhaustion, should the relayer get paid? If so, relayers would be incentivized to send transaction with extremely low amounts of GAS, causing failures in the execution. If not, malicious cross chain users may create infinite cost write operations, making relayers lose money and desincentivizing their work.

To prevent this, all cross chain users must provide a execution cost $C_{\mathtt{execution}}$ and a reward $C_{\mathtt{reward}}$ (which will be locked in the cross chain management contract), along with the destination blockchain $M_{\mathtt{dst}}$ and the write operation $Q_{\mathtt{write}}$, to the cross chain management contract when initiating a cross chain write operation. Then the relayer will get paid only if the following conditions are met:

1. The write operation $Q_{\mathtt{write}}$ is sent to the destination blockchain $M_{\mathtt{dst}}$ by the transaction $X_{\mathtt{dst}}$
2. The transaction $X_{\mathtt{dst}}$ is paid with the execution cost $C_{\mathtt{execution}}$ (or the gas limit is the execution cost $C_{\mathtt{execution}}$)

Note that the sucess or failure of $Q_{\mathtt{write}}$ is not taken into account when deciding if the relayer should get paid and that only the first relayer to send the write operation $Q_{\mathtt{write}}$ will get paid.

Futhermore, optimization could be introduced here that paying relayer on the destination chain to avoid the cost of relayer for read operation by the cross chain management contract of the source chain and help relayer get paid faster than previous design. The concrete realization is that the relayer could call the cross chain management conract of the destination chain to release the token of reward which is locked on the source chain besides normal operation as mentioned above. Thus, the relayer could be paid with the token once it fullfills its responsibility correctly.

### Who will act as the Smart Contract Caller on the Destination Chain when performing cross chain calls?

A naive solution, illustrated in the following diagram, would be to make the relayer be the caller of the cross chain management contract, with the caller of the real contract being the cross chain management contract.

```{.mermaid caption="A Naive Solution of Cross Chain Contract Call"}
graph LR
    R[Relayer]
    subgraph DST_CHAIN
        C[Real Contract]
        M[Cross Chain Management Contract]
    end
    R-- Q -->M
    M-->C
```

The problem with this solution is that all the cross chain users will share the same caller, which is unsecure. There are at least 3 solutions for this problem:

1. Have the destination chain support proxy calls natively, which would allow for the caller, sender and even payer of a transaction to be different. While this may be a perfect solution, there are almost no chains that support this currently.
2. Publish a standard that prohibits the use of the contract caller as an authentication mechanism in smart contracts. This solution is not friendly nor compatible with most smart contracts.
3. Have the cross chain management contract create a intermediate contract for each cross chain user and then use the intermediate contract to call the real contract.

We choose the third solution, illustrated in the following diagram.

```{.mermaid caption="Intermediate Contract for Cross Chain Contract Call"}
graph LR
    R[Relayer]
    subgraph DST_CHAIN
        C[Real Contract]
        M[Cross Chain Management Contract]
        I[Intermediate Contract]
    end
    R-- Q -->M
    M-- Q -->I
    I-->C
```

This solution involves having the cross chain management contract create an intermediate contract that acts as an account for each cross chain user. Then, each write operation will use the cross chain user's own intermediate contract to perform the call, essentially turning such contract into a proxy of the cross chain user.

### How to know whether a Cross Chain Write Operation has been executed successfully?

This can be achieved by validating the state root in the block that contains the transaction $X_{\mathtt{dst}}$.

- If the validating party is a user or a relayer in the destination blockchain $M_{\mathtt{dst}}$, he can get the results directly without requiring any cross chain operation.
- If it's a user in the source blockchain $M_{\mathtt{src}}$ instead, he can get the results by means of a cross chain read operation, which will be discussed in the next section.

### Write Operation Summary

This section will summarize our write operation solution and provide an overview of our cross chain interoperability process.

`USER` in the diagram is the cross chain user.
`SRC_MANAGER` is the cross chain management contract on the source chain.
`RELAYER` is the relayer.
`DST_MANAGER` is the cross chain management contract on the destination chain.
`INTERMEDIATE` is the intermediate contract for the cross chain user created by the cross chain management contract on the destination chain.

1. Initiate

   To initiate a cross chain write operation on the source chain, `USER` sends a transaction $X_{\mathtt{src}}$ on the source chain. The transaction $X_{\mathtt{src}}$ must call `SRC_MANAGER` and provide the following information:

   - destination blockchain $M_{\mathtt{dst}}$
   - write operation $Q_{\mathtt{write}}$
   - execution cost $C_{\mathtt{execution}}$
   - reward $C_{\mathtt{reward}}$

2. Hunt

   After transaction $X_{\mathtt{src}}$ is executed successfully, `SRC_MANAGER` will log the following information:

   - destination blockchain $M_{\mathtt{dst}}$
   - write operation $Q_{\mathtt{write}}$
   - execution cost $C_{\mathtt{execution}}$
   - reward $C_{\mathtt{reward}}$

   `RELAYER` will then get such infomation by retrieving the log of `SRC_MANAGER` and proceed to the next stage if the reward $C_{\mathtt{reward}}$ is reasonable according to him.

3. Transfer 

   `RELAYER` sends a transaction $X_{\mathtt{dst}}$ on the destination chain. The transaction $X_{\mathtt{dst}}$ must call the `DST_MANAGER` and include the following information:

   - write operation $Q_{\mathtt{write}}$
   - execution cost $C_{\mathtt{execution}}$
   - reward $C_{\mathtt{reward}}$ 
   - inclusion proof 
   - address $A_{\mathtt{relayer}}$ of `RELAYER` in source blockchain $M_{\mathtt{src}}$

   The main function of the proof is to allow the validation of the existence of the log containing the write operation on the source chain. For example, the proof can be a merkle path to be used in SPV ^[Simplified Payment Verification, https://en.bitcoinwiki.org/wiki/Simplified_Payment_Verification].

   The address $a$ can help `RELAYER` prove himself and claim the reward successfully.

4. Execute

   After receiving the proof, `DST_MANAGER` validates the write operation using the block header $H_{\mathtt{src}}^{(t_{\mathtt{src}})}$ on the source chain at block height $t_{\mathtt{src}}$.

   If the validation is successful, `DST_MANAGER` creates an intermediate contract for the cross chain user and makes the intermediate contract execute the write operation $Q_{\mathtt{write}}$.

5. Reward

   `RELAYER` claims the reward from `SRC_MANAGER` by sending a proof, which causes `SRC_MANAGER` to validate the execution by checking the state root in the specific block header that contains the transaction $X_{\mathtt{dst}}$.

   If the validation is successful, the `RELAYER` gets the the reward $C_{\mathtt{reward}}$. Only the first `RELAYER` to finish the cross chain write operation can get the reward, for the `SRC_MANAGER` will log the execution of the write operation.

```{.mermaid caption="Cross Chain Write Operation"}
sequenceDiagram
    participant U as USER
    participant S as SRC_MANAGER
    participant R as RELAYER
    participant D as DST_MANAGER
    participant I as INTERMEDIATE
    U->>S: M, Q, Ce, Cr
    S-->>R: M, Q, Ce, Cr
    alt accept?
        R->>D: Q, Ce, Cr, proof
        Note right of D: validate
        alt valid?
            D->>I: Q
            Note right of I: execute Q
            R->>S: ask for pay
            Note right of S: validate
            alt valid?
                S->>R: pay
            else
                Note right of S: do nothing
            end
        else
            Note right of D: do nothing
        end
    else
        Note right of R: do nothing
    end
```

## Read Operation Solution

Similar to the write operation solution, the main idea for the read operation solution is that a relayer will help the cross chain user transfer the status on the destination chain to the source chain, where the status returned will be validated, allowing for the role of relayer to not require any trust.

The details of each step will be discussed in the following sections.

```{.mermaid caption="Read Operation Overview"}
graph RL
    S[SRC CHAIN]
    D[DST CHAIN]
    R((RELAYER))
    R-- Status -->S
    D-- Status -->R
```

### How is a Cross Chain Read Operation initiated on the Source Chain?

To initiate a cross chain read operation on the source chain, the cross chain user should send a transaction $X_{\mathtt{src}}$ there, calling the cross chain management contract and providing the destination blockchain $M_{\mathtt{dst}}$ and the read operation $Q_{\mathtt{read}}$, which will be logged by the contract.

After the cross chain management contract is called, the transaction $X_{\mathtt{src}}$ is confirmed on the source chain $M_{\mathtt{src}}$ at the block height $t_{\mathtt{src}}$, which means,

$$
X_{\mathtt{src}} \in \mathbf{S}_{\mathtt{src}}^{(t_{\mathtt{src}})}
$$

The destination chain $M_{\mathtt{dst}}$ and the read operation $Q_{\mathtt{read}}$ can be inferred from the transaction $X_{\mathtt{src}}$ after which successful execution.

Example:

1. A cross chain user calls a contract on the source chain.
2. The contract needs to know the cross chain user's balance on the destination chain, so it calls the cross chain management contract and tells it the destination blockchain $M_{\mathtt{dst}}$ and the read operation $Q_{\mathtt{read}}$.

```{.mermaid caption="Initiate Read Operation"}
graph TD
    U[Cross Chain User]
    subgraph SRC_CHAIN
        C[Contract]
        M[Cross Chain Management Contract]
    end
    U-->C
    C-- M_dst, Q -->M
```

### How does the relayer get the Cross Chain Read Operation from the Source Chain?

Only if the transaction $X_{\mathtt{src}}$ is executed successfully, the cross chain management contract will log the destination blockchain $M_{\mathtt{dst}}$ and the read operation $Q_{\mathtt{read}}$, which will be obtained by a relayer when it retrieves the log of the cross chain management contract. In other words, the relayer gets such infomation from the output $O_{\mathtt{src}}^{(t_{\mathtt{src}})}$ of the source chain at block height $t_{\mathtt{src}}$. 

The following diagram shows the relayer retrieving the destination blockchain $M_{\mathtt{dst}}$ and the read operation $Q_{\mathtt{read}}$ from the cross chain management contract:

```{.mermaid caption="Obtain Read Operation"}
graph LR
    R[Relayer]
    subgraph SRC_CHAIN
        M[Cross Chain Management Contract]
    end
    M-- M_dst, Q -->R
```

### How does the relayer get the Status of the Destination Chain?

When a relayer gets a cross chain read operation from the source chain, the destination blockchain $M_{\mathtt{dst}}$ and the read operation $Q_{\mathtt{read}}$ will be extracted.

Afterwards, the relayer will infer the block height $t_{\mathtt{dst}}$ from the read operation $Q_{\mathtt{read}}$, then proceed the retrieve the blockchain state at block height $t_{\mathtt{dst}}$ on the destination chain $M_{\mathtt{dst}}$. Finally, it will use the output function ${f_{\mathtt{output}}}_{\mathtt{dst}}$ to obtain the output of the destination chain $M_{\mathtt{dst}}$ and get the specific view of the output requested by $Q_{\mathtt{read}}$.

### How does the relayer send the Status to the Source Chain?

The relayer sends the status via a call to the cross chain management contract on the source chain, which will validate it and, if real, log it, making it available to the cross chain user.

```{.mermaid caption="Transfer Status"}
graph LR
    R[Relayer]
    subgraph SRC_CHAIN
        M[Cross Chain Management Contract]
    end
    R-- status -->M
```

### How is the Status sent by the relayer validated?

When the cross chain management contract is called by the relayer, who must provide both the status and a proof of its inclusion on the destination chain, the contract will first check the validity of the status provided using the block header $H_{\mathtt{dst}}^{(t_{\mathtt{dst}})}$ on the destination chain at the block height $t_{\mathtt{dst}}$.

Then, for the source chain, how to retrieve the block header $H_{\mathtt{dst}}^{(t_{\mathtt{dst}})}$ on the destination chain at the block height $t_{\mathtt{dst}}$? In this section, we just assume that the source chain can do it. We will discuss it in the [Synchronize Block Header](#Synchronize-Block-Header) section.

But how can a contract on the source chain retrieve the block header $H_{\mathtt{dst}}^{(t_{\mathtt{dst}})}$ on the destination chain? The system used to obtain such capability will be discussed in section [Synchronize Block Header](#Synchronize-Block-Header), for now it will just be assumed it's possible.

### Cost and Incentives

A transaction fee should be paid when executing transactions in any blockchain, and given that the solution discussed involves two transactions on the source chain, one sent by the crosschain user and the other by the relayer, these must be paid for.

To prevent money loss on the relayer side, when the cross chain user initiates a cross chain read operation on the source chain, the user can set a reward for relayer, which will be awarded to the first relayer that returns a valid status back.

Putting everything together: the cross chain user provides the destination blockchain $M_{\mathtt{dst}}$, the read operation $Q_{\mathtt{read}}$ and the reward $C_{\mathtt{reward}}$ (the reward assets should be locked in the contract) to the cross chain management contract when initiating a cross chain read operation. Then the relayer gets paid only after the correct status is sent to the source chain.

Note that only the first relayer to transfer a valid status for $Q_{\mathtt{read}}$ will get paid.

### Read Operation Summary

This section will summarize our write operation solution and provide an overview of our cross chain interoperability process.

The `USER` in the diagram is the cross chain user.
The `SRC_MANAGER` is the cross chain management contract on the source chain.
The `RELAYER` is the relayer.

1. Initiate

   To initiate a cross chain read operation on source chain, `USER` sends a transaction $X_{\mathtt{src}}$ on the source chain. The transaction $X_{\mathtt{src}}$ must call the `SRC_MANAGER` and contain the following information:

   - destination blockchain $M_{\mathtt{dst}}$
   - read operation $Q_{\mathtt{read}}$
   - reward $C_{\mathtt{reward}}$

2. Hunt

   Only after transaction $X_{\mathtt{src}}$ is executed successfully, `SRC_MANAGER` will log the following information:

   - destination blockchain $M_{\mathtt{dst}}$
   - read operation $Q_{\mathtt{read}}$
   - reward $C_{\mathtt{reward}}$

   `RELAYER` will then get such infomation by retrieving the log of `SRC_MANAGER` and proceed to the next stage if the reward $C_{\mathtt{reward}}$ is reasonable according to him.


3. Execute

   The relayer will infer the block height $t_{\mathtt{dst}}$ from the read operation $Q_{\mathtt{read}}$, then proceed the retrieve the blockchain state at block height $t_{\mathtt{dst}}$ on the destination chain $M_{\mathtt{dst}}$. Finally, it will use the output function $g_{\mathtt{dst}}$ to obtain the output of the destination chain $M_{\mathtt{dst}}$ and get the specific view of the output requested by $Q_{\mathtt{read}}$.

4. Return

   The relayer sends the status via a call to the cross chain management contract on the source chain, which will validate it and, if real, log it, making it available to the cross chain user.

5. Validate

   After receiving the proof and status, `SRC_MANAGER` validates the read operation by checking the block header $H_{\mathtt{src}}^{(t_{\mathtt{src}})}$ on the source chain at the block height $t_{\mathtt{src}}$.

6. Reward

   `RELAYER` claims the reward from `SRC_MANAGER` by sending a proof, which causes `SRC_MANAGER` to validate the status by checking the state root in the specific block header that was requested in $Q_{\mathtt{read}}$.

   If the validation is successful, the `RELAYER` gets the the reward $C_{\mathtt{reward}}$. Only the first `RELAYER` to finish the cross chain read operation can get reward, for the `SRC_MANAGER` will log the execution of read operation.

```{.mermaid caption="Cross Chain Read Operation"}
sequenceDiagram
    participant U as USER
    participant S as SRC_MANAGER
    participant R as RELAYER
    participant D as DST_CHAIN
    U->>S: M, Q, Cr
    S-->>R: M, Q, Cr
    alt accept?
        D-->>R: status
        R->>S: status, proof
        Note right of S: validate
        alt valid?
            Note right of S: log status
            S->>R: pay
        else
            Note right of D: do nothing
        end
    else
        Note right of R: do nothing
    end
```

## Synchronize Block Header

We have previosuly assumed that the block headers of both destination and source chains will be available to each other, as the verification process of the wite operation involves retrieving a block header of the source chain from the destination chain, and the inverse process is performed when verifying a read operation. This section will explain how this is achieved.

A baseline solution for header synchronization is to have the source chain and the destination chain synchronize their block headers with each other. If this is done, there should be initial trust between the source chain and the destination chain at the start of the synchronization, due to the fact that the first block headers will have to be hardcoded.

But, after this initial synchronization, this solution would be mostly trustless, having only two main problems:

- As an SPV-based solution it'd be vulnerable to the attack in which the validators/miners create illegal block headers, which the other chain would accept as valid. With no effect on their own chain, the attack could be launched by current validators or previous validators if the sychronization from participant chain to relay chain is not timely. The assumption of validators' kindness are not supposed to be expanded, as they only need to be responsible for their own chain instead of all the chains. Actually, the attack is a kind of fork in some sense, which may cause more serious problems in cross chain circumstances.

- If there are $n_{\mathcal{M}}$ blockchains that want to interoperate with each other, totally there will be $C_{n_{\mathcal{M}}}^2$ sequences of block headers to be synchronized in every chain, therefore adding storage, transaction and processing costs that burden the source chain and grow with O(n), where n is the number of chains that can communicate via crosschain with the source chain. The cost could be very high considering the size and number of block header of current blokcchains.

To solve these issues, we propose a solution based on a relay chain that will synchronize all the participant chain's block headers so that the participant chains only needs to stay synchronized with the relay chain's header, from which the participant chain's block headers could be obtained, as they would have been collected and put in blocks by the relay chain previously. The relay chain can prevent every participant chain from forking through its governance model. In additon, each node of relay chain are supposed to run full nodes of all participant chains to guarantee timeliness of synchronization between relay chain and participant chains. We proceed to fix the security issue through forcing the consensus nodes of the relay chain to run full nodes in every blockchain supported by the crosschain protocol. That way the nodes will perform full verification on every block so that if an illegal block was to appear they would reject it. This solutions allows us to lower the costs on each of the chains to just O(1) (as we only synchronize a single block header indepedently of the amount of chains in the system). 

We formally define the output $\mathbb{O}_{\mathtt{O}}$ of the relay chain $M_{\mathtt{O}}$ the following way:

$$
\mathbb{O}_{\mathtt{O}} = \bigcup_{i \in \mathcal{M}} \mathbb{H}_i^m
$$

We use $\mathbb{H}_i$ to represent the domain of block headers of each blockchain. Therefore, the output of the relay chain is a sequence of the headers of each blockchain.

Then, given that it's possible to use the block header $H$ to prove some parts of a blockchain's status, validation can be performed using the relay chain's header $H_{\mathtt{O}}$ to prove the existence of participant chain's header $H_i, i \in \mathcal{M}$. Then, we can use $H_i$ to prove the status of the blockchain $i$, enabling the participant chain to do cross chain validation after synchronizing the relay chain's header.

Furthermore, the relay chain that we proposed will have following features:

- In opposition to other existing cross chain solutions like Cosmos ^[A Network of Distributed Ledgers, Jae Kwon, Ethan Buchman] and Polkadot ^[POLKADOT: VISION FOR A HETEROGENEOUS MULTI-CHAIN FRAMEWORK, DR. GAVIN WOOD], we define the relay chain as a tokenless consortium blockchain.
- The consensus algorithm of the relay chain will be BFT-like consensus, therefore forks can theoretically be avoided.
- The relay chain can prevent every participant chain from forking through its governance model. For example, the relay chain can set a challenge period and detect the any forking blockchain, in order to punish it.

Wrapping everything up, the relay chain is the coordinator of the entire multi-chain architecture, managing the registration, change, and cancellation of participant chains. Any participant chain can submit a registration request to the relay chain and, if the audit passes, become a member of the entire chain network system.

Further details on the relay chain workings will be publilshed in a future governance whitepaper.

```{.mermaid caption="Relay Chain Synchronizes All Block Headers of Participant Chains"}
graph BT
    R[Relay Chain]
    A[ChainA]
    B[ChainB]
    C[ChainC]
    R---A
    R---B
    R---C
```

Finally, it's possible to optimize this solution even further, which could be divided into the following two parts.

### Synchronize Block Header from Participant Chain to Relay Chain

The blocks of relay chain could only store the hash of block headers of participant chains, and the respective block headers could be provided when a crosschain call need them, as each node of relay chain extra run full nodes of paticipatinf chains.
In this system, validation would be performed by getting $H_{\mathtt{O}}$, obtaining the hash of the participant chain's header through an SPV proof, retrieving the real participant chain header $H_i$, confirming its authenticity by checking its hash and then applying the SPV proofs that have been discussed earlier.

### Synchronize Block Header from Relay Chain to Participant Chain

Participant chains could only synchronize the key block header of relay chain, which records the change of validators of relay chain. Thus, validation could be performed by check the signature of validators of relay chain.

However, in case that previous validators of relay chain launch attack descirbed previously in baseline solution, there should be a merkle tree composed of all the current merkle root in block header of relay chain, and the root of this merkle tree is also stored in block header of relay chain. Thus, the block history of relay chain could be only for all the participant chains. 

## Atomic Cross Chain Transactions

As the scale of business expands, the scenario of interchain information interaction becomes more complex and requires the support of atomic cross chain transactions, that is, multiple operations of a single transaction are performed between different chains with the condition that, if the execution on one of these chains fails, the transaction as a whole fails to execute. Making it so the transaction can be considered successful only if the execution of all operations on all the different chains is successful. But, how can we ensure that all chain operations succeed or fail at the same time, given that this involves transactional issues of cross chain operations?

We propose a two-phase commit protocol for atomic cross chain transactions based on cross chain write operations. This protocol is not a core part of the system presented in this paper but an extension of it, as its implementation is not required for everything else to work. Nevertheless, it provides a solution to smart contracts that need atomic cross chain transactions.

### Two-Phase Contract

To implement atomic cross chain transactions, we use a two-phase commit protocol which will be implemented by the cross chain contract as two phases:

- `prepare` phase

  `prepare` phase executes atomic cross chain transactions and locks related resources in participant chains. It will return a success or failure flag to the relay chain indicating the result of the execution. In addition, for `prepare` to indicate a success it needs to ensure that the `commit` and `rollback` phases would succeed if they were to be excuted later on.

- `commit`/`rollback` phase

  `commit` phase unlocks related resources and finalizes all states of an atomic cross chain transaction after receiving the commit command from relay chain. `rollback` phase rolls back all states of an atomic cross chain transaction after receiving the roll back command from relay chain.

The life cycle of an atomic crosschain transaction is as follows:

1. An atomic cross chain transaction is initiated and sent to some chains. 
2. The `prepare` function in each one of these chains executes the transactions, locks related resources and sends a success/failure message to the relay chain. 
3. Relay chain determines whether the execution is successful in every participant chain. If so, the commit command will be sent to participant chains and the `commit` function on each participant chain will submit all states of the transaction after unlocking the related resources. If not, the rollback command will be sent to participant chains and the `rollback` function of each participant chain will roll back all states of the atomic cross chain transaction in that participant chain.

#### An Example of a Successful Atomic Cross Chain Transaction

While the proposed protocol can support more chains than two, for the sake of simplicity there will only be 2 destination chains in the following example.

- `RELAYER` is the relayer.
- `RELAY_CHAIN` is the relay chain
- `DST_CHAIN_1` is the first destination chain
- `DST_CHAIN_2` is the second destination chain

1. An atomic cross chain transaction $X_{\mathtt{ato}}$ is initiated on source chain $M_{\mathtt{src}}$, which calls the cross chain management contract on source chain, and provides the following information. Only if $X_{\mathtt{ato}}$ is executed successfully the following data will be logged. Then such information will be retrieved by relayer.

   - destination blockchains `DST_CHAIN_1` and `DST_CHAIN_2`
   - write operations $Q_1$ and $Q_2$
   - execution costs ${C_{\mathtt{execution}}}_1$ and ${C_{\mathtt{execution}}}_2$
   - rewards ${C_{\mathtt{reward}}}_1$ and ${C_{\mathtt{reward}}}_2$

2. `RELAYER` sends a transaction on `DST_CHAIN_1` to execute the `prepare` phase of $Q_1$ through write operation. The execution cost and the reward are included in ${C_{\mathtt{execution}}}_1$ and ${C_{\mathtt{reward}}}_1$. Then the cross chain management contract on `DST_CHAIN_1` will validate the atomic cross transaction with the block header $H_{\mathtt{src}}^{(t_{\mathtt{src}})}$ on the source chain at the block height $t_{\mathtt{src}}$ and use the `intermediate` contract to execute it.
3. The execution in `DST_CHAIN_1` is successful, and the two-phase contract's `prepare` function locks all related resources and returns success. The success of `prepare` phase of $Q_1$ is synchronized to the relay chain, which means all related resources are locked and `commit`/`rollback` phase will never fail.
4. `RELAYER` sends a transaction on `DST_CHAIN_2` to execute the `prepare` phase of $Q_2$ through write operation. The execution cost and the reward are included in ${C_{\mathtt{execution}}}_2$ and ${C_{\mathtt{reward}}}_2$. Then the cross chain management contract on `DST_CHAIN_2` will validate the atomic cross transaction with the block header $H_{\mathtt{src}}^{(t_{\mathtt{src}})}$ on the source chain at the block height $t_{\mathtt{src}}$ and use the `intermediate` contract to execute it.
5. The execution in `DST_CHAIN_2` is successful, and the two-phase contract's `prepare` function locks all related resources and returns success. The success of `prepare` phase of $Q_2$ is synchronized to the relay chain, which means all related resources are locked and `commit`/`rollback` phase will never fail.
6. On relay chian, `RELAYER` can get the proof of that the `prepare` phases on all destination chains are succeed.
7. `RELAYER` sends a transaction with the previous proof on `DST_CHAIN_1` to execute the `commit` phase of $Q_1$. The execution cost and the reward are included in ${C_{\mathtt{execution}}}_1$ and ${C_{\mathtt{reward}}}_1$.
8. `RELAYER` sends a transaction with the previous proof on `DST_CHAIN_2` to execute the `commit` phase of $Q_2$. The execution cost and the reward are included in ${C_{\mathtt{execution}}}_2$ and ${C_{\mathtt{reward}}}_2$.

```{.mermaid caption="Example of a Successful Atomic Cross Chain Transaction Process"}
sequenceDiagram
    participant R as RELAYER
    participant O as RELAY_CHAIN
    participant D1 as DST_CHAIN_1
    participant D2 as DST_CHAIN_2
    Note left of R: get atomic txs
    R->>D1: prepare
    Note right of D1: success
    D1-->>O: prepared
    R->>D2: prepare
    Note right of D2: success
    D2-->>O: prepared
    Note right of O: all the prepare success
    O-->>R: get proof of all success
    R->>D1: commit
    Note right of D1: commit done
    R->>D2: commit
    Note right of D2: commit done
```

#### An Example of a Failed Atomic Cross Chain Transaction

1. Steps 1-4 are the same as in the previous example. 
2. The `prepare` phase of $Q_2$ on `DST_CHAIN_2` fails and the fail of `prepare` phase of $Q_2$ is synchronized to the relay chain.
3. On relay chain, `RELAYER` can get the proof of that all `prepare` phases on destination chains are succeed.
4. `RELAYER` sends a transaction with the previous proof on `DST_CHAIN_1` to execute the `rollback` phase of $Q_1$. The execution cost and the reward are included in ${C_{\mathtt{execution}}}_1$ and ${C_{\mathtt{reward}}}_1$.
5. The execution of `rollback` phase of $Q_1$ is synchronized to the relay chain at end. 

```{.mermaid caption="Example of a Failed Atomic Cross Chain Transaction Process"}
sequenceDiagram
    participant R as RELAYER
    participant O as RELAY_CHAIN
    participant D1 as DST_CHAIN_1
    participant D2 as DST_CHAIN_2
    Note left of R: get atomic txs
    R->>D1: prepare
    Note right of D1: success
    D1-->>O: prepared
    R->>D2: prepare
    Note right of D2: fail
    D2-->>O: failed
    Note right of O: not all prepare success
    O-->>R: get proof of not all success
    R->>D1: rollback
    Note right of D1: rollback done
    D1-->>O: rollbacked
```

### Relay Chain as a Coordinator

To help participant chains know whether the `prepare` phase is executed successfully, the relay chain should maintain a table of prepare-phase statuses for each atomic cross chain transaction and provide two functions to notify whether the `prepare` phase is executed successfully on a participant chain: function `success` $f_{\mathtt{success}}$ and function `fail` $f_{\mathtt{success}}$. We use $\mathbf{\Lambda}$ to represent the data stored in the relay chain for an atomic cross chain transaction, data that will be modeled as a n-length vector of $-1$, $0$, and $1$ where n is the number of destination chains and $-1$, $0$, $1$ represent `successful`, `unknown`, `failed` respectively. 

$$
\mathbf{\Lambda} \in {\left \{ -1, 0, 1 \right \}}^n
$$

We use $\mathbf{0}^{n}$ to denote a n-length vector populated by $0$, $\mathbf{1}^{(k)}$ to denote a vector in which the $k$-th item is $1$ and all the other items are $0$ and $\mathbf{-1}^{(k)}$ to denote a vector in which the $k$-th item is $-1$ and all other items are $0$. 

The two functions mention beforehand obey the following rules:

- `success`

  When the `prepare` phase runs successfully on blockchain $M_k$, it can tell the relay chain that the `prepare` phase is executed successfully on blockchain $M_k$ using this function $f_{\mathtt{success}}$.

  $$
  f_{\mathtt{success}}(\mathbf{\Lambda}, k) = 
  \begin{cases}
      \mathtt{map}_{\mathtt{max}}(\mathbf{\Lambda}, \mathbf{1}^{(k)})   & \mathtt{min}(\mathbf{\Lambda}) > -1 \\
      \mathbf{\Lambda} & \mathtt{else} \\
  \end{cases}
  $$

- `fail`

  When the `prepare` phase fails on blockchain $M_k$ or the `rollback` phase is executed, the function $f_{\mathtt{fail}}$ will be called.

  $$
  f_{\mathtt{fail}}(\mathbf{\Lambda}, k) = 
  \begin{cases}
      \mathtt{map}_{\mathtt{min}}(\mathbf{\Lambda}, \mathbf{-1}^{(k)}) & \mathtt{min}(\mathbf{\Lambda}) < 1 \\
      \mathbf{\Lambda} & \mathtt{else} \\
  \end{cases}
  $$

$\mathbf{\Lambda}$ is $\mathbf{0}^{n}$ initially. The function `success` $f_{\mathtt{success}}$ and function `fail` $f_{\mathtt{success}}$ can update the data $\mathbf{\Lambda}$, which means the next state of $\mathbf{\Lambda}$ can be the result of $f_{\mathtt{success}}$ or $f_{\mathtt{fail}}$:

$$
\mathbf{\Lambda}^{(t+1)} =
\begin{cases}
    f_{\mathtt{success}}(\mathbf{\Lambda}^{(t)}, k) \\
    f_{\mathtt{fail}}(\mathbf{\Lambda}^{(t)}, k) \\
\end{cases}
$$

When the atomic cross chain transaction is initially lauched on the source chain, the relayer helps transfer this transaction to multiple destination chains and the relay chain. Then, the relay chain creates a new $\mathbf{\Lambda} = \mathbf{0}^n$, which will be modified after the `prepare` phase is executed in the multiple destination chains, whether the `prepare` phase has been executed successfully in each destination chain can be synchronized to the relay chain. Afterwards, if all the `prepare` phases have been executed successfully, the changes will be commited gradually in the multiple destination chains by the `commit` phase. If one of the `prepare` phases ends up in failure, then the relayers will help all the destination chains execute the `rollback` phase.

The same incentives for relayers explained in previous sections are applied directly when performing atomic crosschain transactions.

## Optimization

Some optimization strategies are introduced below, which could be alternatives for the final solution or could be combined with futher optimization.

### Time Out

To improve the operation efficiency of cross chain transactions and ensure token liquidity, the time out feature, which could be viewed as execution deadline, on cross chain transactions set by cross chain users is proposed. This feature could especially benefit cross chain users, for example, in the previous solution without time out feature, after initiating cross chain transactions, cross chain users need to wait until relayers execute them, which may cause long lead time for cross chain execution and higher uncertainty for locked rewards in the cross chain contract. The details of time out feature in different cross chain operations are illustrated below: 

- Write Operation

  Cross chain users could set a time out feature when issuing a new cross chain write operation. This time out feature could be monitored and determined by the specific block height on the destination chain: if the current block height is lower than the time out, this cross chain write operation could still be executed and rewards set by cross chain users will remain locked; while the block height is higher than the time out, the cross chain management contract on the destination chian will raise an error and this cross chain write operation will be expired and no further execution would be permitted. Besides, the user's rewards will be unlocked and taken back if someone gives a proof on the source chain that the transaction is not executed before time out. 
   
- Read Operation

  Cross chain user could set a time out when initiating the cross chain read operation, similar to the write operation, this time out feature could be monitored and determined by the specific block height on the source chain: if the current block height is lower than the time out, this cross chain read operation could still be executed and rewards set by cross chain users will remain locked; if higher than the time out, the cross chain management contract on the source chain will raise an error and user's rewards will be unlocked and be taken back if no status return on the source chain after time out.

- Atomic Cross Chain Transaction

  Cross chain user could set a time out when initiating atomic cross chain transactions, too. This time out feature could be monitored and determined by the specific block height on the relay chain. If the current block height is lower than the time out, only function `fail` can be called on the relay chain.

### Relayer Protection

There may exist some risks for relayers of the current solution. 

Firstly, based on "the first relayer is the winner" rule, block proposer ^[Community Yellow Paper: A Technical Specification for NEO Blockchain, Igor M. Coelho, Vitor N. Coelho, Peter Lin, Erik Zhang], which is one of consensus nodes to generate and send new blocks, could cheat on relayers by copying cross chain transactions submitted by relayers with a new address and rank their transactions ahead of relayers. In this situation, it is hard to prove who is the first executor and whether consensus nodes change the order of blocks or not, so relayers may lose transation fees and get no reward. To reduce this risk, the optional solution is proposed here: when a relayer identifies a cross chain transaction opportunity on the source chain, he could deposit some tokens, the amount and the type of which will be specified by cross chain users.

The deposit information will be logged and the cross chain management contract can only allow the relayer who is bound to transfer the cross chain operation. Other relayers will stop executing this cross chain transaction. This method could largely reduce relayers' transaction fee losses of not being the winner. If someone proves that the cross chain operation is not confirmed before time out, all of the locked token can be used to compensate the cross chain user.

Secondly, even if adding the time out feature, the block proposer could still cheat on relayers by refusing to proceed cross chain transactions until the time out expires. Since it is hard to prove whether relayers should be blamed or the consensus nodes should be blamed, relayers may finally need to suffer from transaction fee losses. When the relayer promises to execute this cross chain operation, he should carefully evaluate and be responsible for all potential risks. Actually it's not a issue only for cross chain but also blockchain transactions because common transactions. (e.g. assets transfer and contract call maybe delayed by the block proposers, too.) However, block proposers are replaced frequently in most blockchains (even in each block). If one of the block proposers is upright, the cross chain operation will be transfered correctly.

# The Analysis

The analysis of our solution is divided in several parts as following including safety, latency, TPS and cost.

## Safety

Safety properties informally require that "something bad will never happen". ^[Rodrigues, Christian Cachin; Rachid Guerraoui; Luís (2010). Introduction to reliable and secure distributed programming (2. ed.). Berlin: Springer Berlin. pp. 22–24. ISBN 978-3-642-15259-7.] ^[Lamport, L. (1977). "Proving the Correctness of Multiprocess Programs". IEEE Transactions on Software Engineering (2): 125–143. CiteSeerX 10.1.1.137.9454. doi:10.1109/TSE.1977.229904.] ^[Alpern, B.; Schneider, F. B. (1987). "Recognizing safety and liveness". Distributed Computing. 2 (3): 117. CiteSeerX 10.1.1.20.5470. doi:10.1007/BF01782772.]

The premise of our discussion of safety is that the participant chains are functioning properly. Otherwise, even if the cross chain solution provides the correct information, the information may still be tampered with or discarded due to abnormal operation of the participant chain.

We mentioned in the previous sections that have a trusted relay chain and the governance mechanism of the relay chain provide us non-fork participant chains. In our solution, the status in the output of relay chain can be hash of the header of participant chain $\mathtt{hash}(H_{\mathtt{participant}})$ and the key header $K_{\mathtt{O}}$ of relay chain is stored in the participant chain which can be obtained by the cross chain management contract.

The following proof can ensure that a status of a participant chain on another participant chain is valid.

$$
O_{\mathtt{status}} \leftarrow H_{\mathtt{participant}} \leftarrow \mathtt{hash}(H_{\mathtt{participant}}) \leftarrow H_{\mathtt{O}} \leftarrow K_{\mathtt{O}}
$$

- $O_{\mathtt{status}} \leftarrow H_{\mathtt{participant}}$ : header can prove the status (SPV Proof) ^[http://docs.electrum.org/en/latest/spv.html] ^[Nakamoto, Satoshi (24 May 2009). "Bitcoin: A Peer-to-Peer Electronic Cash System" (PDF). Retrieved 20 December 2012.]
- $H_{\mathtt{participant}} \leftarrow \mathtt{hash}(H_{\mathtt{participant}})$ : A cryptographic hash function allows one to easily verify whether some input data map onto a given hash value, but if the input data is unknown it is deliberately difficult to reconstruct it (or any equivalent alternatives) by knowing the stored hash value. ^[Schneier, Bruce. "Cryptanalysis of MD5 and SHA: Time for a New Standard". Computerworld. Retrieved 2016-04-20. Much more than encryption algorithms, one-way hash functions are the workhorses of modern cryptography.] ^[Al-Kuwari, Saif; Davenport, James H.; Bradford, Russell J. (2011). "Cryptographic Hash Functions: Recent Design Trends and Security Notions".]
- $\mathtt{hash}(H_{\mathtt{participant}}) \leftarrow H_{\mathtt{O}}$ : header can prove the status and the status on relay chain is headers on each participant chain (SPV Proof) ^[http://docs.electrum.org/en/latest/spv.html] ^[Nakamoto, Satoshi (24 May 2009). "Bitcoin: A Peer-to-Peer Electronic Cash System" (PDF). Retrieved 20 December 2012.]
- $H_{\mathtt{O}} \leftarrow K_{\mathtt{O}}$ : common headers can be verified by the signature of the majority* of validators metioned in the key header ^[Paul, Eliza (12 September 2017). "What is Digital Signature- How it works, Benefits, Objectives, Concept". EMP Trust HR.]

### Safety in Synchronizing Block Header

Safety in synchronizing block chain means that respective block header could be synchronized to participant chains and relay chain correctly.

For synchronizing block header from participant chain to relay chain, as each node of relay chain runs extra full node of all participant chains, they could synchronize block header of participant chain and store hash of block header in relay chain correctly and in time.

For synchronizing block header from relay chain to participant chain, nodes of relay chain are supposed to sychronize key block header of relay chain to participant chain in time, which records the change of validators of relay chain. Actually the merkle root of previous block headers on relay chian is written in key block header which avoid long range attacks. ^[Deirmentzoglou, Evangelos, Georgios Papakyriakopoulos, and Constantinos Patsakis. "A Survey on Long-Range Attacks for Proof of Stake Protocols." IEEE Access 7 (2019): 28712-28725.]

### Safety of Write Operation

Safety of write operation means that forged cross chain write operation would never be executed on the destination chain by relayers and fake relayers can never get the reward of write operation.

Actually all the cross chain write operations will be validated before it is executed. The cross chain management contract will ensure that the write operation $Q_{\mathtt{write}}$ has been comfirmed successfully on the source chain because the write operation $Q_{\mathtt{write}}$ is a status in output of the source chain and it can be verified by the destination chain. Then the forged write operation will be denied by the cross chain management contract.

Similarly, the relayer will be paid only if their transaction $X_{\mathtt{dst}}$ is confirmed on the destination chain. Actually the block $H_{\mathtt{dst}}$ header can also help to validate the state of a blockchain and the $X_{\mathtt{dst}}$ can be validated.

### Safety of Read Operation

Safety of read operation means that forged status cannot be returned back to the cross chain user and only if the relayer returned the correct status, the relayer can get the reward of read operation. Actually all the status can be verified by the source chain by the block header that we discussed before and the cross chain management contract ensures that after the status is verified, the relayer gets paid.

### Safety of Unrelated Participant Chain

Safety of unrelated participant chain means that result of a transaction between source chain and destination chain wouldn't have effect on other paticipant chains, a.k.a. unrelated participant chain.

In our solution, realy chain is supposed to help participant chains synchronize blokc header, and transactions between source chain and destination chain are handled just by these two chain. Thus, unrelated participant chains wouldn't be affected. 

## Latency

We use latency to evaluate the delay from a cross chain user to initiate a cross chain operation to the cross chain operation being executed.

We can not ensure that any cross chain operation would eventually be exectued in a static finite time because whether the operation is finished depends on the relayer. Incentives can increase the enthusiasm of the relayer to speed up trading. The more rewards for the relayer, the lower latency of the cross chain transactions. It is a kind of cross chain transaction fee (just like the transaction fee in many blockchains, which can promote the transaction to be packaged into the block by the consensus node).

Actually there may be some optimized solution to improve the latency of the cross chain solution in the future work.

### Write Operation Latency

To discuss the latency of write operation $d_{\mathtt{write}}$, we first use the following notations:

- $d_{\mathtt{write},\mathtt{src}}$

  delay from the transaction $X_{\mathtt{src}}$ being sent on the source chain by user to the transaction $X_{\mathtt{src}}$ being confirmed at the block $I_{\mathtt{src}}^{(t_{\mathtt{src}})}$ on the source chain

- $d_{\mathtt{sync},\mathtt{write}}$

  delay from the transaction $X_{\mathtt{src}}$ being confirmed at the block $I_{\mathtt{src}}^{(t_{\mathtt{src}})}$ on the source chain to enough information for the proof being synchronized.

- $d_{\mathtt{write},\mathtt{relayer}}$

  delay from the transaction $X_{\mathtt{src}}$ being confirmed on the source chain to the transaction $X_{\mathtt{dst}}$ that including the write operation $Q_{\mathtt{write}}$ being sent on the destination chain by relayer

- $d_{\mathtt{write},\mathtt{dst}}$

  delay from transaction $X_{\mathtt{dst}}$ being sent on the destination chain by relayer to the transaction $X_{\mathtt{dst}}$ being confirmed at the block $I_{\mathtt{dst}}^{(t_{\mathtt{dst}})}$ on the relay chain

The formula of latency of write operation being executed $d_{\mathtt{write}}$ could be described as 

$$
d_{\mathtt{write}} =
d_{\mathtt{write},\mathtt{src}}
+
\mathtt{max}
\begin{cases}
d_{\mathtt{sync},\mathtt{write}} \\
d_{\mathtt{write},\mathtt{relayer}} + d_{\mathtt{write},\mathtt{dst}} \\
\end{cases}
$$

Actually we have the following constraint, otherwise the validation must be failed.

$$
d_{\mathtt{write},\mathtt{relayer}} + d_{\mathtt{write},\mathtt{dst}} \geqslant d_{\mathtt{sync},\mathtt{write}}
$$

Then

$$
d_{\mathtt{write}} = d_{\mathtt{write},\mathtt{src}} + d_{\mathtt{write},\mathtt{relayer}} + d_{\mathtt{write},\mathtt{dst}}
$$

- The transaction $X_{\mathtt{src}}$ may be executed and confirmed within a very short period if the time of it being sent is close to generation of next block ($d_{\mathtt{write},\mathtt{src}}$ could be close to zero). However, the transaction may also wait for a lone time if source chain is in congestion. 

  $$
  d_{\mathtt{write},\mathtt{src}} > 0
  $$

- If validators of relay chain don't change and they synchronize hash of block header of source chain once the transaction $X_{\mathtt{src}}$ is confirmed, $d_{\mathtt{sync},\mathtt{write}}$ could be close to zero. However, if validators of relay chain change and sychroniztion of key block header is delayed becasuse of some reasons such as congestion of destination chain, $d_{\mathtt{sync},\mathtt{write}}$ could be very high. Thus, the range of $d_{\mathtt{sync},\mathtt{write}}$ is greater than zero:

  $$
  d_{\mathtt{sync},\mathtt{write}} > 0
  $$

- If relayer detects the transaction $X_{\mathtt{src}}$ once it is confirmed on the source chain and sends the transaction $X_{\mathtt{dst}}$ immediately, $d_{\mathtt{write},\mathtt{relayer}}$ could be close to zero. If the transaction $X_{\mathtt{src}}$ is not detected timely or its reward is not high enough, respective $d_{\mathtt{write},\mathtt{relayer}}$ could be very high. Thus, the range of $d_{\mathtt{write},\mathtt{relayer}}$ is greater than zero:

  $$
  d_{\mathtt{write},\mathtt{relayer}} > 0
  $$

- Similar to $d_{\mathtt{write},\mathtt{src}}$, the range of $d_{\mathtt{write},\mathtt{dst}} > 0$ is also greater than zero:

  $$
  d_{\mathtt{write},\mathtt{dst}} > 0
  $$

```{.mermaid caption="Write Operation Latency"}
gantt
    dateFormat ss
    title Write Operation Latency
    section Synchronization
    sync : sync, after src, 40s
    section Operation
    src : src, 2014-01-01, 10s
    relayer : relayer, after src, 34s
    dst :dst, after relayer, 10s
```

### Read Operation Latency

To discuss the latency of read operation $d_{\mathtt{read}}$, we first use the folllowing notations:

- $d_{\mathtt{read},\mathtt{status}}$

  deley from the transaction $X_{\mathtt{src}}$ being sent on the source chain by user to the block $I_{\mathtt{dst}}^{(t_{\mathtt{dst}})}$ on the destination chain specified by the read operation $Q_{\mathtt{read}}$ being confirmed.

- $d_{\mathtt{sync},\mathtt{read}}$

  delay from the transaction the block $I_{\mathtt{dst}}^{(t_{\mathtt{dst}})}$ being confirmed on the destination chain to enough information for the proof being synchronized.

- $d_{\mathtt{read},\mathtt{src}}$

  delay from the transaction $X_{\mathtt{src}}$ being sent on the source chain by user to the transaction $X_{\mathtt{src}}$ being confirmed at the block $I_{\mathtt{src}}^{(t_{\mathtt{src}})}$ on the source chain

- $d_{\mathtt{read},\mathtt{relayer}}$

  delay from the transaction $X_{\mathtt{src}}$ being confirmed on the destination chain to the transaction $X_{\mathtt{return}}$ that including the result of read operation $Q_{\mathtt{read}}$ being sent on the source chain by relayer

- $d_{\mathtt{read},\mathtt{return}}$

  delay from transaction $X_{\mathtt{return}}$ being sent on the source chain by relayer to the transaction $X_{\mathtt{return}}$ being confirmed at the block $I_{\mathtt{src}}^{(t_{\mathtt{return}})}$ on the relay chain

The formula of latency of read operation being executed $d_{\mathtt{read}}$ could be described as 

$$
d_{\mathtt{read}} =
\mathtt{max}
\begin{cases}
d_{\mathtt{read},\mathtt{status}} +
d_{\mathtt{sync},\mathtt{read}} \\
d_{\mathtt{read},\mathtt{src}} + d_{\mathtt{read},\mathtt{relayer}} + d_{\mathtt{read},\mathtt{return}} \\
\end{cases}
$$

Actually we have the following constraint, otherwise the validation must be failed.

$$
d_{\mathtt{read},\mathtt{src}} + d_{\mathtt{read},\mathtt{relayer}} + d_{\mathtt{read},\mathtt{return}} \geqslant d_{\mathtt{read},\mathtt{status}} +
d_{\mathtt{sync},\mathtt{read}}
$$

Then

$$
d_{\mathtt{write}} = d_{\mathtt{read},\mathtt{src}} + d_{\mathtt{read},\mathtt{relayer}} + d_{\mathtt{read},\mathtt{return}}
$$

- Similar with write operation, the $d_{\mathtt{read},\mathtt{src}}$ is larger than 0.

  $$
  d_{\mathtt{read},\mathtt{src}} > 0
  $$

- If the cross chain user want to get an exist status of the destination chain, $d_{\mathtt{read},\mathtt{status}}$ could be 0. If the cross chain user want ot get a status in the future, the delay depends on how many blocks need to wait $t_Q - t_{\mathtt{dst}}$ and the block generation rate $r_{\mathtt{block/time},\mathtt{dst}}$

  $$
  d_{\mathtt{read},\mathtt{status}} = \mathtt{max} \begin{cases}
    \frac{(t_Q - t_{\mathtt{dst}})}{r_{\mathtt{block/time},\mathtt{dst}}} \\
    0 \\
  \end{cases} \geqslant 0
  $$

- If the cross chain user want to get an exist status of the destination chain, $d_{\mathtt{sync},\mathtt{read}}$ could be 0. If the cross chain user want ot get a status in the future, it neead some time to synchronize the block headers

  $$
  d_{\mathtt{sync},\mathtt{read}} \geqslant 0
  $$

- Similar to write operation. $d_{\mathtt{read},\mathtt{relayer}}$ is greater than 0

  $$
  d_{\mathtt{read},\mathtt{relayer}} > 0
  $$

- Similar to write operation. $d_{\mathtt{read},\mathtt{return}}$ is greater than 0

  $$
  d_{\mathtt{read},\mathtt{return}} > 0
  $$

```{.mermaid caption="Read Operation Latency"}
gantt
    dateFormat ss
    title Read Operation Latency
    section Synchronization
    status : status, 2014-01-01, 20s
    sync : sync, after status, 30s
    section Operation
    src : src, 2014-01-01, 10s
    relayer : relayer, after src, 34s
    dst :dst, after relayer, 10s
```

## TPS

The cross chain TPS will be discussed in this section.

The relay chain's TPS could be expressed by the following notations and equation. (Actually the TPS of relay chain would be very high). Firstly, the TPS of relay chain should be larger than $\sum_i {r_{\mathtt{block/time}}}_i$ to maintain smooth operation since $\sum_i {r_{\mathtt{block/time}}}_i$ represents all blocks created per second on participant chain that should be synchronized on relay chain. Secondly, there is a positive correlation between relay chain's TPS ${r_{\mathtt{tx/time}}}_{\mathtt{O}}$ and participant chain's TPS ${r_{\mathtt{tx/time}}}_i$ for $\forall i \in \mathcal{M}$, which means higher TPS on participant chain tends to require higher TPS on relay chain. Thirdly, there is a negative correlation between relay chain's TPS ${r_{\mathtt{tx/time}}}_{\mathtt{O}}$ and participant chain's transactions per block ${r_{\mathtt{tx/block}}}_i$ for $\forall i \in \mathcal{M}$, which means for the certain amount of transactions waiting to be synchronized on participant chain, if there are more transactions per block $\psi_i$, the number of block headers to be synchronized will be less, so the requirement for relay chain's TPS will be lower. 

$$
{r_{\mathtt{tx/time}}}_{\mathtt{O}} > \sum_i {r_{\mathtt{block/time}}}_i = \sum_i \frac{{r_{\mathtt{tx/time}}}_i}{{r_{\mathtt{tx/block}}}_i}
$$

The participant chain's TPS could be expressed by the following notations and equation. Firstly, the TPS of participant chain should be larger than ${r_{\mathtt{key/time}}}_{\mathtt{O}}$, which represents all key blocks created per second on relay chain that should be synchronized on participant chain. Actually ${r_{\mathtt{key/time}}}_{\mathtt{O}}$ is low because the validators on relay chain don't change frequently. So our cross chain solution has almost no requirements for TPS of participant chains.

for $\forall i \in \mathcal{M}$:

$$
{r_{\mathtt{tx/time}}}_i > {r_{\mathtt{key/time}}}_{\mathtt{O}}
$$

## Cost

Finally, we discuss about the cost of cross chian operations including the cost on relay chian, the cost on participant chain, the cost of relayer and the cost of cross chain user.

### Cost on Relay Chain

This section will discuss about the cost of relay chain.

Let $C_{\mathtt{O}}$ be the total cost of relay chain. Assume there are $n_{\mathtt{O}}$ validators on relay chain and each validator spends $C_{\mathtt{O},\mathtt{validator}}$ on operation and maintenance on average. Therefore, the equation of relay chain's total cost will be: 

$$
C_{\mathtt{O}} = n_{\mathtt{O}} C_{\mathtt{O},\mathtt{validator}}
$$

Narrowing down the average cost for each validator, as every validator needs to run both consensus nodes and run all participant chains' sync nodes, $C_{\mathtt{O},\mathtt{validator}}$ could be calculated by the average cost of running a consensus node plus the sum of the cost of running sync nodes of all participant chains: 

$$
C_{\mathtt{O},\mathtt{validator}} = {C_{\mathtt{node},\mathtt{consensus}}}_{\mathtt{O}} + \sum_{i \in \mathcal{M}} {C_{\mathtt{node},\mathtt{sync}}}_i
$$

To simplify the model, it is assumed the average cost of running a node $C_{\mathtt{node}}$, no matter for a consensus node or a sync node, only take the computing ($C_{\mathtt{compute}}$), storage ($C_{\mathtt{storage}}$), and network ($C_{\mathtt{network}}$) fees into consideration.

$$
C_{\mathtt{node}} = C_{\mathtt{compute}} + C_{\mathtt{storage}} + C_{\mathtt{network}}
$$

The details of the three types of cost will be discussed in the following:

1. For the computational capability, let $C_{\mathtt{compute},\mathtt{consensus}}$ be the total computational cost for a consensus node and $C_{\mathtt{compute},\mathtt{sync}}$ be the total computational cost for a sync node. The requirement for running a consensus node on relay chain will usually be higher for running a sync node.

   $$
   C_{\mathtt{compute},\mathtt{consensus}} \gg C_{\mathtt{compute},\mathtt{sync}}
   $$

2. For the storage cost, $C_{\mathtt{storage}}$ equals to unit storage fee $C_{\mathtt{storage},\mathtt{price}}$ times the incremental storage size in unit period $l_{\mathtt{block}} r_{\mathtt{block/time}}$, where $l_{\mathtt{block}}$ represents the block size on average and $r_{\mathtt{block/time}}$ represents the block generation rate on average. The complete equation will be: 
 
   $$
   C_{\mathtt{storage}} = C_{\mathtt{storage},\mathtt{price}} l_{\mathtt{block}} r_{\mathtt{block/time}}
   $$

3. For the network cost, $C_{\mathtt{network}}$ is assumed to be proportional to $C_{\mathtt{storage}}$ with the ratio of $r_{\mathtt{network/storage}}$. The ratio for consensus node is $r_{\mathtt{network/storage},\mathtt{consensus}}$ and ratio for sync node is $r_{\mathtt{network/storage},\mathtt{sync}}$. The $r_{\mathtt{network/storage},\mathtt{sync}}$ could be approximately seen as 1 since sync nodes only do synchronization. Besides, based on BFT consensus algorithm, the $r_{\mathtt{network/storage},\mathtt{consensus}}$ will be generally larger than $r_{\mathtt{network/storage},\mathtt{sync}}$ since validators need to do synchronization, consensus, and broadcasting. Then the estimated equation will be: 

   $$
   C_{\mathtt{network}} \approx r_{\mathtt{network/storage}} C_{\mathtt{network},\mathtt{price}} l_{\mathtt{block}} r_{\mathtt{block/time}}
   $$

   $$
   r_{\mathtt{network/storage},\mathtt{consensus}} \gg r_{\mathtt{network/storage},\mathtt{sync}} \approx 1
   $$

Narrowing down the calculation of storage size on relay chain, there are three main components: the storage of relay chain's block header, the storage of all participant chains' block header hash, and the storage of atomic cross chain transactions. On the one hand, as blocks, so as block headers, are generated at a constant rate ${r_{\mathtt{block/time}}}_{\mathtt{O}}$ on the relay chain, ${l_{\mathtt{header}}}_{\mathtt{O}} {r_{\mathtt{block/time}}}_{\mathtt{O}}$ will represent the storage of relay chain's block header. On the other hand, the body sizes of blocks on relay chain are positively correlated to the block generation rate of participant chains ${r_{\mathtt{block/time}}}_i$, so $\sum_{i \in \mathcal{M}} {l_{\mathtt{hash}}} {r_{\mathtt{block/time}}}_i$ will represent the storage of participant chains' block header hash on the relay chain. Therefore, the equation of the incremental storage size on relay chain will be: 

$$
{l_{\mathtt{block}}}_{\mathtt{O}} {r_{\mathtt{block/time}}}_{\mathtt{O}} = {l_{\mathtt{header}}}_{\mathtt{O}} {r_{\mathtt{block/time}}}_{\mathtt{O}} + \sum_{i \in \mathcal{M}} {l_{\mathtt{hash}}} {r_{\mathtt{block/time}}}_i + l_{\mathtt{atomic}} r_{\mathtt{atomic}}
$$

Therefore, by combining all computational, storage and network cost calculated above, the summary equation of the average cost for each validator $C_{\mathtt{O},\mathtt{validator}}$ will be: 

$$
C_{\mathtt{O},\mathtt{validator}} = \mathtt{sum} \begin{cases}
    {C_{\mathtt{compute},\mathtt{consensus}}}_{\mathtt{O}} \\
    C_{\mathtt{storage},\mathtt{price}} (\mathtt{sum}
    \begin{cases}
        {l_{\mathtt{header}}}_{\mathtt{O}} {r_{\mathtt{block/time}}}_{\mathtt{O}} \\
        \sum_{i \in \mathcal{M}} l_{\mathtt{hash}} {r_{\mathtt{block/time}}}_i \\
        l_{\mathtt{atomic}} r_{\mathtt{atomic}} \\
    \end{cases}) \\
    {r_{\mathtt{network/storage}}}_{\mathtt{O}} C_{\mathtt{netowrk},\mathtt{price}} (\mathtt{sum}
    \begin{cases}
        {l_{\mathtt{header}}}_{\mathtt{O}} {r_{\mathtt{block/time}}}_{\mathtt{O}} \\
        \sum_{i \in \mathcal{M}} l_{\mathtt{hash}} {r_{\mathtt{block/time}}}_i \\
        l_{\mathtt{atomic}} r_{\mathtt{atomic}} \\
    \end{cases}) \\
    \sum_{i \in \mathcal{M}} {C_{\mathtt{compute},\mathtt{sync}}}_i \\
    \sum_{i \in \mathcal{M}} C_{\mathtt{storage},\mathtt{price}} {l_{\mathtt{block}}}_i {r_{\mathtt{block/time}}}_i \\
    \sum_{i \in \mathcal{M}} {r_{\mathtt{network/storage}}}_i C_{\mathtt{network},\mathtt{price}} {l_{\mathtt{block}}}_i {r_{\mathtt{block/time}}}_i \\
\end{cases}
$$

Currently we are going to use specific values to estimate the cost on relay chain. We suppose there are 7 nodes ($|n_{\mathtt{O}}| = 7$) and 10 participant chains ($|\mathcal{M}| = 10$). We estimate the market price and get the unit price of the computing, storage and network ^[https://aws.amazon.com/ec2/pricing/] ^[https://cloud.google.com/pricing/list] ^[https://bitcoin.org/en/full-node#initial-block-downloadibd] (${C_{\mathtt{compute},\mathtt{consensus}}}_{\mathtt{O}} = 10^{-4} \mathtt{\$/s}$, $C_{\mathtt{storage},\mathtt{price}} = 5 \times 10^{-12} \mathtt{\$/b}$, $C_{\mathtt{network},\mathtt{price}} = 5 \times 10^{-12} \mathtt{\$/b}$ and for $\forall i \in \mathcal{M}$, ${C_{\mathtt{compute},\mathtt{sync}}}_i = 10^{-5} \mathtt{\$/s}$). According to the average block generaton rate and the size of block header by BFT algorithm, we estimate some following properties for relay chain ^[https://neoscan.io/blocks/1] ^[https://explorer.ont.io/] (${l_{\mathtt{header}}}_{\mathtt{O}} = 4 \times 10^3 \mathtt{b/block}$, ${r_{\mathtt{block/time}}}_{\mathtt{O}} = 1 \mathtt{block/s}$). We also estimate correlative properties for each participant chain ^[https://en.bitcoin.it/wiki/Protocol_documentation] ^[https://ethereum.github.io/yellowpaper/paper.pdf] ^[https://eostracker.io] ^[https://neoscan.io/blocks/1] ^[https://explorer.ont.io/] (for $\forall i \in \mathcal{M}$, ${r_{\mathtt{block/time}}}_i = 1 \mathtt{block/s}$ and ${l_{\mathtt{block}}}_i = 10^5 \mathtt{b/block}$). We use SHA-256 to get the hash of each block header ^[https://en.wikipedia.org/wiki/SHA-2] ($l_{\mathtt{hash}} = 256 \mathtt{b/block}$). We assume the size for each atomic transaction is $16$ bits ($l_{\mathtt{atomic}} = 16 \mathtt{b/tx}$ and $r_{\mathtt{atomic}} = 1 \mathtt{tx/s}$). And we assume the ratio between the consumption of network and the consumption of storage on relay chain is 100 because of BFT consensus cost and the ratio between the consumption of network and the consumption of storage on participant chain is 1 (${r_{\mathtt{network/storage}}}_{\mathtt{O}} = 100$ and ${r_{\mathtt{network/storage}}}_i = 1$).

As a result, we estimate the following cost for validators in relay chian

$$
C_{\mathtt{O},\mathtt{validator}} = 2.1 \times 10^{-4} \mathtt{\$/s} = 6.6 \times 10^3 \mathtt{\$/year}
$$

### Cost on Participant Chain

participant chain would synchronize key block header of relay chain, in which the change of validators of relay chain is recorded, whose frequency of generation is low. Thus, the cost of participant chain is little enough.

### Cost of Relayer

Relayers are like miners under PoW consensus algorithm. Firstly, relayers get rewards by doing cross chain write and read operations. Secondly, relayers need to compete with each other since only the relayer whose operation is firstly completed and verified would be rewarded. Thirdly, relayers need to bear some costs to conduct cross chain write and read operations.

This section will discuss about the cost of relayer.

The cost of relayers comes from two aspects: the first one is running nodes. From our mechanism, relayers need to run nodes of participant chains, destination chains and relay chain obviously.

Another part is cross chain transaction fee. If relayer want to get the reward set by cross chain user, they need pay for the cross chain transaction in participant chains. We will discuss in detail below

#### Running Nodes

Relayer needs to run the sync node of relay chain and participant chains that realyer would like to help handle cross chain transactions(i.e. respective participant chains of relayer). 

Let $C_{\mathtt{relayer},\mathtt{nodes}}$ be relayer's cost of running nodes, ${\mathcal{M}_{\mathtt{relayer}}}$ be respective participant chains of relayer. Thus, relayer' cost of running nodes could be sum of the cost of running sync nodes of relay chain and respective participant chains:

$$
C_{\mathtt{relayer},\mathtt{nodes}} = \sum_{i \in \mathcal{M}_{\mathtt{relayer}} \cup \{\mathtt{O}\}} {C_{\mathtt{node},\mathtt{sync}}}_i
$$

#### Transaction Fee

Relayers need send transactions which cause transaction fee. In this section, we will talk about the transaction fee of relayers.

1. Write Operation

The transaction fee of write operation for relayers is generated in calling cross chain management contract which is also called execution cost in sections above, and getting reward from cross chain management contract in source chain or destination chain, which can be called demand fee. 

The charge unit of execution cost is the same as destination chain's transaction fee. In addtion, if the cross chain management contract is a native contract ^[https://dev-docs.ont.io/#/docs-en/DeveloperGuide/smartcontract/00-introduction-sc], the execution cost will be determined according to the rules of the native contract, which is cheap for the relief mechanism of transaction fee. Otherwise, we can estimate the execution cost $C_{\mathtt{execution}}$ as follows:

- $C_{Q_{\mathtt{write}}}$ is the cost that should be spent executing the write operations in destination chains
- $C_{\mathtt{intermediate}}$ is the extra cost due to the intermediate contracts
- $C_{\mathtt{validation},\mathtt{write}}$ is the cost of cross chain management contracts to verify the legitimacy of cross chain status

$$
C_{\mathtt{execution}} = C_{Q_{\mathtt{write}}} + C_{\mathtt{intermediate}} + C_{\mathtt{validation},\mathtt{write}}
$$

The charge unit of demand fee depends on the participant chain where relayers get reward. In addtion, if the cross chain management contract is a native contract^[https://dev-docs.ont.io/#/docs-en/DeveloperGuide/smartcontract/00-introduction-sc], the execution cost will be determined according to the rules of the native contract, which is cheap for the relief mechanism of transaction fee. Otherwise, we can estimate the demand fee $C_{\mathtt{getreward}}$ as follows:

- $C_{\mathtt{payreward}}$ is a normal asset transfer fee and usually very low
- $C_{\mathtt{validation},\mathtt{getreward}}$ is the validation cost of cross chain operations. The validation cost can be less if relayers get their reward on the destination chain instead of source chain, because to validate the success excution for source chain is another cross chain read operation.

$$
C_{\mathtt{demand}} = C_{\mathtt{demand}} + C_{\mathtt{validation},\mathtt{demand}}
$$

2. Read Operation

Unlike the transaction fee of write operation, relayers only need to get the status in destination for almost free, then excute a transaction in management contract of source chain with the status and proof. Relayers can only get the reward if the transcation is success. Therefore, relayer need to evaluate the transaction fee and provide the right status.

The charge unit of read fee should be same as source chain's transaction fee. In addtion, if the cross chain management contract is a native contract^[https://dev-docs.ont.io/#/docs-en/DeveloperGuide/smartcontract/00-introduction-sc], the execution cost will be determined according to the rules of the native contract, which is cheap for the relief mechanism of transaction fee. Otherwise, we can estimate the read fee $C_{\mathtt{read}}$ as follows:

- $C_{Q_{\mathtt{read}}}$ is the cost of returning status to source chain and usually depends on the size of the status
- $C_{\mathtt{validation},\mathtt{read}}$ is the validation cost of read operation. To be specific, relayers call the cross chain management contract to validate status and get reward which will generate transaction fee

$$
C_{\mathtt{read}} = C_{Q_{\mathtt{read}}} + C_{\mathtt{validation},\mathtt{read}}
$$

Actually, $C_{\mathtt{intermediate}}$ depends on participant chains, which is about `0.01 GAS` in `NEO` ^[https://neo.org]. The cross chain validation cost $C_{\mathtt{validation}}$ can be estimated by 2 spv proof cost and 1 hash cost and 1 signature check cost, which is about `0.1 GAS` in `NEO` ^[https://neo.org].

### Cost of Cross Chain User

The cost of cross chain user consists of two parts: one is the transaction fee of $X_{\mathtt{src}}$, the other is the reward $C_{\mathtt{reward}}$.

In order to get their cross chain transactions be executed in time and smoothly, except for some special situations that some relayers are willing to serve some users without benefits, cross chain users need to set reasonable and even attractive rewards, covering both transaction costs and premium incentives, for relayers.

In write operation, cross chain users need to at least cover relayer's transaction fees of calling cross chain management contract plus fees of getting rewards, that is:

$$
C_{\mathtt{reward},\mathtt{write}} > C_{\mathtt{execution}} + C_{\mathtt{demand}}
$$

In read operation, cross chain users need to at least pay for relayer's transacton fees of doing read operation, that is:

$$
C_{\mathtt{reward},\mathtt{read}} > C_{\mathtt{read}}
$$

In addition, cross chain users also need to consider the physical cost of the relayer including computing cost, stroage cost, network cost and so on.

Dynamic token exchange rates should be taken into consideration when evaluating cross chain transactions especially in write operation, because relayers need to spend destination chain tokens and be rewarded by other tokens.

<!-- remark
Based on the expected net rewards equation, to be qualified relayers and get enough rewards to support operation, there are some suggestions for relayers. 

1. To execute write and read operations smoothly and successfully, relayers need, on the one hand, be familiar with both source chain and destination chain involved in cross chain transactions, on the other hand, recognize values of token on both chains. 
2. No matter how many cross chain transactions relayers deal with, the $c_{\mathtt{fixed}}$ is fixed. Moreover, relayers could achieve economies of scale by doing more cross chain transactions and reducing their $c_{\mathtt{fixed}}$ to nearly zero. Therefore, relayers need to evaluate the overall demand for cross chain transactions to determine whether and when their economies of scale could be achieved. 

Remark:

There are some problems associated with "the first relayer is the winner" rule. 

1. In write operation, consensus nodes on destination chain may cheat to get rewards by putting their cross chain transactions before other relayers' ones in the block. In read operation, when other relayers are trying to retrieve the blockchain state at block height $t_{\mathtt{dst}}$ on the destination chain $M_{\mathtt{dst}}$, consensus nodes on the destination chain could show status to source chain users directly to get rewards without responding to other relayers. 

2. Consensus nodes on destination chain may favor some relayers they knows and upgrade some relayers' rankings, but no relayers will know that. It will be unfair to other relayers. 

3. If creating a block is a time-consuming process, a large amount of transactions have to wait for confirmation and the probability for each relayer to get rewards will be low.

For source chain users, how to determine rewards $C_{\mathtt{reward}}$ is important. The basic rule is that rewards $C_{\mathtt{reward}}$ should at least be higher than $$p_{\mathtt{write}} + p_{\mathtt{reward}}$$ to cover relayers' transaction fees in write operation, while rewards $C_{\mathtt{reward}}$ should be higher than $p_{\mathtt{read}}$ in read operation. However, there are some uncertainties to be taken into consideration:

1. Time uncertainty exists. Source chain users cannot gurantee when their cross chain transactions could be executed and some risks associated with waiting time will occur. 

2. Exchange rates uncertainty between tokens on source chain and destination chain exists. In write operation, relayers' transaction fees are calculated by destination chain tokens, users could pay relayers with destination chain or source chain tokens. In reading operation, it is more convenient to pay with source chain tokens. 
-->

# The Conclusion

In this cross chain project, we outlined our core structure based on Read and Write operation to implement the interoperation between blockchains. This project can remove the barrier of information silos and improve the user experience when using and developing DApps. We have given out a precise proof for the safety of this cross chain project. We roughly talked about some new strategies to optimize our cross chain solution for further development. We have drafted an economic model for incentivizing relayers to make our network better. We are devoting ourselves for a better blockchain network, and we hope our project will make a big step for the development of Web 3.0.